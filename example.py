# convention: exp(i omega t)
#             epsilon_0=mu_0=1
# periodic boundary condition with k=0
# PML can be added

import numpy as np
import cupy as cp
from cmath import pi
from time import time
import h5py

from operators import oper_obj
from tool import interp, write_results
from solver import Solver
output_ep = 0
output_E = 0
#------------- passing argument---------------#
freq = 1.
omega = freq*2*pi+0*1j

# overal computational domain, number of grid points
Nx = 200
Ny = 200
Nz = 200
Nxyz = (Nx,Ny,Nz)
print("The size of the problem is ",Nxyz)

# pixel size
hx = 0.02
hy = 0.02
hz = 0.02
hxyz = (hx,hy,hz)

# region for structuring
Mx = 50
My = 50
Mz = 50
Mzslab = 1
ndof=Mx*My

# PML
Npmlx = 10
Npmly = 10
Npmlz = 10
Npmlxyz = (Npmlx,Npmly,Npmlz)
m=4.0
R=16.0

# setup epsilon profile
epsbkg = 1.
epsdiff = 1.
dof=np.ones([Mx,My]).astype(np.float)
epsBkg = np.ones(Nxyz)*epsbkg
Mx0= int((Nx-Mx)/2)
My0= int((Ny-My)/2)
Mz0= int((Nz-Mz)/2)
A = interp(Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0)

ep1 = np.copy(epsBkg)
A.matA(dof,ep1,epsdiff)
if output_ep == 1:
    f=h5py.File('epsF.h5','w')
    f.create_dataset('data',data=ep1)
    f.close()

# setup current source
Jdir = 2
Jamp = 1.
cx = Nx//2
cy = Ny//2
cz = Nz//2
J = [np.zeros(Nxyz,dtype=complex) for i in range(3)]
J[Jdir][cx,cy,cz]=Jamp/hx/hy #electric current
b=[-1j*omega*np.copy(z) for z in J] #b=-i omega j

# setup operators and solve for E
bd = np
ope = oper_obj(bd,Nxyz,Npmlxyz,hxyz,omega,m=m,R=R,debug=0)
solve1 = Solver(ope,err_thresh=1e-4,outputbase=10)
solve1.operator.update_ep(ep1)
xout, err, success = solve1.Multisolve(b)

if output_E == 1:
    xcpu = [f.get() for f in xout]
    write_results('E.h5',xcpu)
