from tool import PML, reshaper, apply_cond
import numpy as np

class sEHobj():
    def __init__(self,sE,sH,bd,Nxyz):
        self.sE = sE
        self.sH = sH
        self.bd = bd
        self.Nxyz = Nxyz

        self.sEm = reshaper(sE,Nxyz)
        self.sHm = reshaper(sH,Nxyz)
        
        sqrtsE = reshaper([bd.sqrt(f) for f in sE],Nxyz)
        sqrtsH = reshaper([bd.sqrt(f) for f in sH],Nxyz)
        self.post_cond = lambda x:apply_cond(x,sqrtsE,sqrtsH)

        invsqrtsE = [bd.reciprocal(f) for f in sqrtsE]
        invsqrtsH = [bd.reciprocal(f) for f in sqrtsH]
        self.pre_cond = lambda x:apply_cond(x,invsqrtsE,invsqrtsH)
        
class oper_obj():
    def __init__(self,bd,Nxyz,Npmlxyz,hxyz,omega,m=4.,R=16.,debug=0):
        self.bd = bd
        self.Nxyz = Nxyz
        self.hxyz = hxyz
        self.omega = omega
        
        # assembly PML
        pmlx=PML(Npmlxyz[0],omega,Nxyz[0],hxyz[0],m,R)
        pmly=PML(Npmlxyz[1],omega,Nxyz[1],hxyz[1],m,R)
        pmlz=PML(Npmlxyz[2],omega,Nxyz[2],hxyz[2],m,R)

        pml_p=[pmlx.sp*hxyz[0],pmly.sp*hxyz[1],pmlz.sp*hxyz[2]]
        pml_d=[pmlx.sd*hxyz[0],pmly.sd*hxyz[1],pmlz.sd*hxyz[2]]

        self.sE = [1./bd.array(f) for f in pml_p]
        if debug == 0:
            self.sH = [1./bd.array(f) for f in pml_d]
        else:
            self.sH = [1./bd.array(f) for f in pml_p]

        self.sEH = sEHobj(self.sE,self.sH,bd,Nxyz)

    def update_ep(self,ep,minv=None):
        '''
        ep is a (Nx,Ny,Nz) numpy array
        '''
        if minv == None:
            minv = [self.bd.ones(self.Nxyz) for i in range(3)]
        epw2 = [self.bd.array(ep)*self.omega**2 for i in range(3)]

        self.alphafun = lambda p:alpha_fun(self.bd,p,self.sEH,epw2,minv)

    def rho_step(self,alpha,p,r,v,x):
        """ Return the function to execute the rho step of the bicg algorithm.
        rho_step -- rho_step(alpha, p, r, v, x) updates r and x, and returns
        rho and the error. Specifically, rho_step performs:
            x = x + alpha * p
            r = r - alpha * v
            rho_(k+1) = (r dot r)
            err = (conj(r) dot r)
        """
        x = [x[i] + alpha*p[i] for i in range(3)]
        r = [r[i] - alpha*v[i] for i in range(3)]
        rho = self.bd.sum(r[0]**2+r[1]**2+r[2]**2)
        err = self.bd.sum(self.bd.abs(r[0])**2+self.bd.abs(r[1])**2+self.bd.abs(r[2])**2)
        return r,x,rho,self.bd.sqrt(err)

    def alpha_step(self,rho,rhom,p,r): 
        """ Define the alpha step function needed for the bicg algorithm.
        alpha_step -- alpha_step(rho_k, rho_(k-1), p, r, v) updates p and v, and 
        returns alpha. Specifically, alpha_step performs:
            p = r + (rho_k / rho_(k-1)) * p
            v = A * p
            alpha = rho_k / (p dot v)
        """
        p = [r[i] + rho/rhom*p[i] for i in range(3)]
        v = self.alphafun(p)
        alpha = rho/self.bd.sum(p[0]*v[0]+p[1]*v[1]+p[2]*v[2])
        return p,v,alpha        
            
       
def alpha_fun(bd,P,sEH,epw2,minv):
    Eout = sEH.post_cond(P)

    Mout = M_mult(bd,Eout,sEH,epw2,minv)
    Pout = sEH.pre_cond(Mout)

    return Pout


def M_mult(bd,E,sEH,epw2,minv):
    '''
    Periodic boundary condition is assumed.
    M E = (curl muinv curl - epw2)E = (sE D muinv sH D - epw2)E

    bd being either numpy or cupy

    epw2 = e*omega^2, e = [ex,ey,ez], each is 3D array of [x,y,z]
    
    E = [Ex,Ey,Ez], each is  3D array of [x,y,z]
    s = [sx,sy,sz], scaling factor
        sH for H, and sE for E
    '''

    padf = (((0,1),(0,0),(0,0)),  ((0,0),(0,1),(0,0)),  ((0,0),(0,0),(0,1)))
    padb = (((1,0),(0,0),(0,0)),  ((0,0),(1,0),(0,0)),  ((0,0),(0,0),(1,0)))
    DE = lambda x,pol,ax:bd.diff(bd.pad(x[pol],padf[ax],'wrap'),axis=ax)*sEH.sHm[ax]
    DH = lambda x,pol,ax:bd.diff(bd.pad(x[pol],padb[ax],'wrap'),axis=ax)*sEH.sEm[ax]

    #### !!!!! Below H is not magnetic field, but just muinv*curl E
    #### Forward difference
    #Hx = minvx (dEzdy-dEydz)
    Hx = minv[0]* (DE(E,2,1) - DE(E,1,2))

    #Hy = minvy (dExdz - dEzdx)
    Hy = minv[1]* (DE(E,0,2) - DE(E,2,0) )

    #Hz = minvz (dEydx - dExdy)
    Hz = minv[2]* (DE(E,1,0)-DE(E,0,1))

    H= [Hx,Hy,Hz]
    ##########
    #### Backward difference
    #Mx = (dHzdy - dHydz) - epw2[x]Ex
    Mx = DH(H,2,1) - DH(H,1,2) - epw2[0] * E[0]
    
    #My = (dHxdz - dHzdx) - epw2[y]Ey
    My = DH(H,0,2) - DH(H,2,0) - epw2[1] * E[1]
    
    #Mz = (dHydx - dHxdy) - epw2[z]Ez
    Mz = DH(H,1,0)- DH(H,0,1) - epw2[2] * E[2]

    return [Mx,My,Mz]
