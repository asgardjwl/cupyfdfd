import numpy as np
import time

class Solver:
    def __init__(self, operator,err_thresh=1e-4,max_iters=40000,verbose=1,outputbase=1000,init=10,init_type='zero'):
        self.operator = operator
        self.bd = operator.bd
        self.err_thresh = err_thresh
        self.max_iters = max_iters
        self.verbose = verbose
        self.outputbase = outputbase
        self.cout = 0
        self.init = init
        self.init_type = init_type
        self.shape = operator.Nxyz
        self.zeros = lambda : [self.bd.zeros(self.shape) for i in range(3)]
        
    def bicg(self, r, x=None):
        # Note: r is used instead of b in the input parameters of the function.
        # This is in order to initialize r = b, and to inherently disallow access to 
        # b from within this function.

        # Initialize variables. 
        # Note that r = b was "initialized" in the function declaration.
        
        """ Lumped bi-conjugate gradient solve of a symmetric system.

        Input variables:
        b -- the problem to be solved is A * x = b.

        Keyword variables:
        x -- initial guess of x, default value is 0.
        rho_step -- rho_step(alpha, p, r, v, x) updates r and x, and returns
        rho and the error. Specifically, rho_step performs:
        x = x + alpha * p
        r = r - alpha * v
        rho_(k+1) = (r dot r)
        err = (conj(r) dot r)
        alpha_step -- alpha_step(rho_k, rho_(k-1), p, r) updates p and v, and 
        returns alpha. Specifically, alpha_step performs:
        p = r + (rho_k / rho_(k-1)) * p
        v = A * p
        alpha = rho_k / (p dot v)
        zeros -- zeros() creates a zero-initialized vector. 
        err_thresh -- the relative error threshold, default 1e-6.
        max_iters -- maximum number of iterations allowed, default 1000.

        Output variables:
        x -- the approximate answer of A * x = b.
        err -- a numpy array with the error value at every iteration.
        success -- True if convergence was successful, False otherwise.
        """

        # Initialize x = 0, if defined.
        if x is None: # Default value of x is 0.
            x = self.zeros()

        # Initialize v = Ax.
        _,v,_ = self.operator.alpha_step(1,1,x,self.zeros())

        p = self.zeros() # Initialize p = 0.
        alpha = 1 # Initial value for alpha.

        rho = self.bd.zeros(self.max_iters,dtype=complex)
        rho[-1] = 1 # Silly trick so that rho[k-1] for k = 0 is defined.

        err = self.bd.zeros(self.max_iters,dtype=float)

        _,_,temp,b_norm = self.operator.rho_step(0,p,r,v,x)
    
        print('b_norm check: ', b_norm) # Make sure this isn't something bad like 0.
        t1 = time.time()
        
        # iteration for the solver
        for k in range(self.max_iters):
            r,x,rho[k], err0 = self.operator.rho_step(alpha, p, r, v, x) 
            err[k] = err0/b_norm

            # Check termination condition.
            if self.verbose>=2:
                t2 = time.time()
                print("   k=",k,", time=",t2-t1,",  err=",err[k])
            elif self.verbose<2 and k%self.outputbase is 0:
                t2 = time.time()
                print("   k=",k,", time=",t2-t1,",  err=",err[k])

            if err[k] < self.err_thresh: # We successfully converged!
                t2 = time.time()
                print("---k=",k,", time=",t2-t1,",  err=",err[k])
                return x, err[:k+1], True

            p,v,alpha = self.operator.alpha_step(rho[k], rho[k-1], p, r)

        # Return the answer, and the progress we made.
        t2 = time.time()
        print("---k=",k,"  err=",err[k])
        return x, err, False

    def Multisolve(self, b, x=None):
        bg = [self.bd.array(f) for f in b]
        bcond = self.operator.sEH.pre_cond(bg)
        
        # first solve
        ctrl=0
        x, err, success = self.bicg(bcond,x)
        xout = self.operator.sEH.post_cond(x)

        return xout, err, success

        # if success:
        #     return x, err, success
