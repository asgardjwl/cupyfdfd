import cmath
import numpy as np
import h5py

def reshaper(f,Nxyz):
    for k in range(3):
        new_shape = [1, 1, 1]
        new_shape[k] = Nxyz[k]
        f[k] = f[k].reshape(new_shape)
    return f
    
def apply_cond(x, t0, t1):
    yx = x[0] * t1[0] * t0[1] * t0[2]
    yy = x[1] * t0[0] * t1[1] * t0[2]
    yz = x[2] * t0[0] * t0[1] * t1[2]
    return [yx,yy,yz]

def write_results(filename,x):
    f=h5py.File(filename,'w')
    for k in range(3):
        data=np.real(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'r',data=data)

        data=np.imag(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'i',data=data)
    f.close()
    
class PML:
    def __init__(self, NPML, omega, N, hx, m=4.0, R=16.0):
        if NPML>0:
            sigma=(m+1)/2/(NPML*hx)*R
            xd=np.linspace(1, N, N)*hx
            xp=xd-0.5*hx

            # prime
            lower=NPML*hx-xp
            upper=xp-(N-NPML)*hx

            l = lower*(lower>0)+upper*(upper>0);
            self.sp=1-1j*(sigma / omega) * (l / NPML/ hx)**m;
        
            # dual
            lower=NPML*hx-xd;
            upper=xd-(N-NPML)*hx;

            l = lower*(lower>0)+upper*(upper>0);
            self.sd=1-1j*(sigma / omega) * (l / NPML/hx)**m;
        else:
            self.sp=np.ones(N)
            self.sd=np.ones(N)

class interp:
    def __init__(self,Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0,rep=1):
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mzslab=Mzslab
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        self.rep = rep
        
    def matA(self,dof,ep,epsdiff):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                for kx in range(self.rep):
                    for ky in range(self.rep):
                        ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i] += dof*epsdiff
        else:
            for kx in range(self.rep):
                for ky in range(self.rep):
                    for kz in range(self.rep):            
                        ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep] += dof*epsdiff
    def matAT(self,dof,e,factor=1.):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                for j in range(3):
                    for kx in range(self.rep):
                        for ky in range(self.rep):
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i]
        else:
            for j in range(3):
                for kx in range(self.rep):
                    for ky in range(self.rep):
                        for kz in range(self.rep):                
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep]

